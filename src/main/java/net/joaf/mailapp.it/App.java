package net.joaf.mailapp.it;

/**
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public class App {
    public static void main( String[] args ) {
        System.out.println( "Empty source - Integration Test project!\nSee: src/test package." );
    }
}
