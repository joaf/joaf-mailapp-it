Feature: Send single email using REST and OAuth
  Scenario: Successful send and receive email
    Given application id a1daf344
    Given application secret aaa333
    Given email server localhost, user test@test.host identified by password testpassword
    Given email template is sample_template
    Given email model is {"test_field":"Test value"}
    Given I am on the front page
    When request for send email
    Then no error occured
    Then service will return success status
#    Then email will be received in 1000 miliseconds
