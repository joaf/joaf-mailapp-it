package net.joaf.mailapp.it.features;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Component
public class SendEmailStepdefs {

	private List<String> exceptions = new ArrayList<>();
	private String result = "";

	private WebDriver webDriver;

	private String applicationId;
	private String applicationSecret;
	private String emailTemplate;
	private Map<String,String> emailModel = new HashMap<>();


	public SendEmailStepdefs() {
		this.webDriver = new SharedDriver();
	}

	@Given("^I am on the front page$")
	public void i_am_on_the_front_page() throws InterruptedException {
		webDriver.get("http://localhost:" + 8080);
	}


	@When("request for send email")
	public void request_for_send_email() {
		try {
			//run oauth
			//run API
			//store result
			result = "unknown";
		} catch (Exception e) {
			exceptions.add(e.getLocalizedMessage());
		}
	}

	@Given("^application id (.+)$")
	public void application_id(String arg1) throws Throwable {
		// Express the Regexp above with the code you wish you had
		this.applicationId = arg1;
	}

	@Given("^application secret (.+)$")
	public void application_secret(String arg1) throws Throwable {
		// Express the Regexp above with the code you wish you had
		this.applicationSecret = arg1;
	}

	@Given("^email server localhost, user test@test.host identified by password testpassword$")
	public void email_server_localhost_user_test_test_host_identified_by_password_testpassword() throws Throwable {
		// Express the Regexp above with the code you wish you had
//		throw new PendingException();
	}

	@Given("^email template is (.+)$")
	public void email_template_is_sample_template(String arg1) throws Throwable {
		this.emailTemplate = arg1;
	}

	@Given("^email model is (.+)$")
	public void email_model_is_(String arg1) throws Throwable {
		// Express the Regexp above with the code you wish you had
//		throw new PendingException();
	}

	@Then("^no error occured$")
	public void no_error_occured() throws Throwable {
		assertTrue(exceptions.isEmpty());
	}

	@Then("^service will return (.*) status$")
	public void service_will_return_status(String status) throws Throwable {
		String resultCode = result;
		assertEquals(resultCode, status);
	}
}
